package com.saswati.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DemoController {
	@RequestMapping(value="/")
	public String home(){
		return "home";
	}
	
	@RequestMapping(value="/index",method= RequestMethod.POST)
	public String index(@RequestParam String username,ModelMap map){
		map.addAttribute("username",username);
		return "index";
	}
}
